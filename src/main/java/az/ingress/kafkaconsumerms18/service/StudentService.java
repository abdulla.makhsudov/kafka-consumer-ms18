package az.ingress.kafkaconsumerms18.service;

import az.ingress.kafkaconsumerms18.listener.KafkaConsumer;
import az.ingress.kafkaconsumerms18.model.Student;
import az.ingress.kafkaconsumerms18.repo.StudentRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentService {

    private final StudentRepo studentRepo;

    @KafkaListener(topics = {"ms18TopicObject"}, containerFactory = "kafkaJsonListenerContainerFactory")
    public void listenerMessage(Student student) {
        Student save = studentRepo.save(student);
        log.info("Saved user: {}", save);
    }
}
