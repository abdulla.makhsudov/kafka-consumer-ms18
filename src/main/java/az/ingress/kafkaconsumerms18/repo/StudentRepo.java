package az.ingress.kafkaconsumerms18.repo;

import az.ingress.kafkaconsumerms18.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepo extends JpaRepository<Student, Long> {
}
