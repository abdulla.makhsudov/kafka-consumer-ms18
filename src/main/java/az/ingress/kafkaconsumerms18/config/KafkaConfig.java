package az.ingress.kafkaconsumerms18.config;

import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;

//@Configuration
//@EnableKafka
//public class KafkaConfig {
//
//    public Map<String, Object> props() {
//        Map<String, Object> props = new HashMap<>();
//        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
//        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringSerializer.class);
//        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringSerializer.class);
//        props.put(ConsumerConfig.GROUP_ID_CONFIG, "ms18-1");
//        return props;
//    }
//
//    @Bean
//    ConsumerFactory<String, String> consumerFactory() {
//        return new DefaultKafkaConsumerFactory<>(props());
//    }
//
//    @Bean
//    public ConcurrentKafkaListenerContainerFactory<String, String> listenerContainerFactory() {
//        ConcurrentKafkaListenerContainerFactory<String, String> containerFactory =
//                new ConcurrentKafkaListenerContainerFactory<>();
//        containerFactory.setRecordMessageConverter(new StringJsonMessageConverter());
//        containerFactory.setConsumerFactory(consumerFactory());
//        return containerFactory;
//    }
//}
