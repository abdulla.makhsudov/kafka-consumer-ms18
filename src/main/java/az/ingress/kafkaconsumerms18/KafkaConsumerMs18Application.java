package az.ingress.kafkaconsumerms18;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaConsumerMs18Application {

    public static void main(String[] args) {
        SpringApplication.run(KafkaConsumerMs18Application.class, args);
    }

}
