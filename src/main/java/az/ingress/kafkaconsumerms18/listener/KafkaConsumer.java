package az.ingress.kafkaconsumerms18.listener;

import az.ingress.kafkaconsumerms18.model.Student;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaConsumer {

    @KafkaListener(topics = {"ms18Topic"}, containerFactory = "kafkaJsonListenerContainerFactory")
    public void listenerMessage(String message) {
        log.info("Message received: {}", message);
    }

    @KafkaListener(topics = {"ms18TopicObject"}, containerFactory = "kafkaJsonListenerContainerFactory")
    public void listenerMessage(Student student) {
        log.info("Object message received : {}", student);
    }
}
